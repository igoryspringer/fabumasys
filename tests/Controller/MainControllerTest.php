<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MainControllerTest extends WebTestCase
{
    public function testMainPage()
    {
        $client = static::createClient();

        $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/profile');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $client->request('GET', '/communal');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $client->request('GET', '/category');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $client->request('GET', '/login');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/logout');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}
