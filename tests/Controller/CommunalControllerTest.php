<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CommunalControllerTest extends WebTestCase
{
    public function testListCommunal()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/communal');
        $this->assertEquals(301, $client->getResponse()->getStatusCode());
    }

    public function testShowCommunal()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/communal/108');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateCommunal()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $crawler = $client->request('GET', '/communal/new');
        $buttonCrawlerNode = $crawler->selectButton('Submit');
        $form = $buttonCrawlerNode->form([
           'communal[name]' => 'TEST',
           'communal[price]' => '101',
           'communal[category]' => '124',
           ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testUpdateCommunal()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $crawler = $client->request('GET', '/communal/edit/108');
        $buttonCrawlerNode = $crawler->selectButton('Submit');
        $form = $buttonCrawlerNode->form([
            'communal[counter]' => '1111',
            'communal[quantity]' => '1111',
            'communal[category]' => '124',
        ]);
        $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testDeleteCommunal()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/communal/delete/108');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}
