<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegisterControllerTest extends WebTestCase
{
    public function testRegister()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $crawler = $client->request('GET', '/register');
        $ButtonCrawlerNode = $crawler->selectButton('Submit');
        $form = $ButtonCrawlerNode->form([
            'users[email]' => '1@2.3',
            'users[plainPassword][first]' => '123',
            'users[plainPassword][second]' => '123',
            'users[roles]' => 'ROLE_USER',
        ]);
        $client->submit($form);

        $client->request('GET', '/register');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
