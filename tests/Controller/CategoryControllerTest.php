<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryControllerTest extends WebTestCase
{
    public function testListCategory()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/category');
        $this->assertEquals(301, $client->getResponse()->getStatusCode());
    }

    public function testShowCategory()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/category/124');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateCategory()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $crawler = $client->request('GET', '/category/new');
        $buttonCrawlerNode = $crawler->selectButton('Submit');
        $form = $buttonCrawlerNode->form([
           'category[name]' => 'TEST',
           'category[unit]' => 'TEST',
           //'category[parentId]' => '',
           ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testUpdateCategory()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $crawler = $client->request('GET', '/category/edit/125');
        $buttonCrawlerNode = $crawler->selectButton('Submit');
        $form = $buttonCrawlerNode->form([
            'category[name]' => 'TEST_edit',
            'category[unit]' => 'TEST_edit',
        ]);
        $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testDeleteCategory()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'admin@admin.ua',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/category/delete/125');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}
