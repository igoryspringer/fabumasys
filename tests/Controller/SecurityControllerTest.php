<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testlogin()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/login');
        $ButtonCrawlerNote = $crawler->selectButton('Sign in');
        $form = $ButtonCrawlerNote->form([
            'email' => 'user@user.ua',
            'password' => 'user',
        ]);
        $client->submit($form);

        $client->request('GET', '/profile');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
