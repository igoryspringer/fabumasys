<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Communal;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommunalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
            ])
            /*->add('unit', TextType::class, [
                'required' => true,*/
            ->add('unit', HiddenType::class, [
                'required' => false,
            ])
            ->add('price', TextType::class, [
                'required' => true,
            ])
            ->add('counter', TextType::class, [
                'required' => false,
            ])
            ->add('quantity', TextType::class, [
                'required' => false,
            ])
            ->add('period', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    'Jan' => 1,
                    'Feb' => 2,
                    'Mar' => 3,
                    'Apr' => 4,
                    'May' => 5,
                    'Jun' => 6,
                    'Jul' => 7,
                    'Aug' => 8,
                    'Sep' => 9,
                    'Oct' => 10,
                    'Nov' => 11,
                    'Des' => 12,
                ],
            ])
            ->add('total', TextType::class, [
                'required' => false,
            ])
            ->add('status', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    'New' => 0,
                    'Payed' => 1,
                ],
            ])
            ->add('iddocument', TextType::class, [
                'required' => false,
            ])
            ->add('datedocument', DateType::class, [
                'required' => false,
                'widget' => 'single_text',
                //'input' => 'text',
                //'attr' => ['class' => 'js-datepicker'],
                //'html5' => false,
            ])
            /*->add('category', HiddenType::class, [
                'required' => false,
            ])*/
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                /*function(Category $category) {
                    return sprintf('(%d) %s', $category->getId(), $category->getName());
                },*/
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Communal::class,
        ]);
    }
}
