<?php

namespace App\Repository;

use App\Entity\Communal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Communal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Communal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Communal[]    findAll()
 * @method Communal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommunalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Communal::class);
    }

    /**
     * @return Communal[] Returns an array of Communal objects
     */
    public function findBySearchField($value): array
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT c
            FROM App\Entity\Communal c
            WHERE (
            c.name LIKE :val
            OR c.price LIKE :val
            OR c.counter LIKE :val
            OR c.quantity LIKE :val
            OR c.total LIKE :val
            OR c.iddocument LIKE :val
            OR c.datedocument LIKE :val
            )
            ORDER BY c.created_at DESC'
        )->setParameter('val', '%'.$value.'%');
        // returns an array of Product objects
        return $query->getResult();
    }

    public function findByCategoryId($value): array
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT c
            FROM App\Entity\Communal c
            WHERE (
            c.category = :val
            )
            ORDER BY c.id DESC'
        )->setParameter('val', $value);
        // returns an array of Product objects
        return $query->getResult();
    }

    public function findAllCategoryId(): array
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT c.id
            FROM App\Entity\Communal c
            JOIN App\Entity\Category ct
            WHERE c.category = ct.id
            ORDER BY c.id ASC'
        );
        //'SELECT a FROM CmsArticle a JOIN a.user u ORDER BY u.name ASC'
        // returns an array of Product objects
        return $query->getResult();
    }

    // /**
    //  * @return Communal[] Returns an array of Communal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Communal
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
