<?php

declare(strict_types=1);

namespace App\Event;

use App\Entity\Users;
use Symfony\Contracts\EventDispatcher\Event;

class RegisteredUserEvent extends Event
{
    public const NAME = 'users.register';

    /**
     * @var Users
     */
    private $registeredUser;

    public function __construct(Users $registeredUser)
    {
        $this->registeredUser = $registeredUser;
    }

    public function getRegisteredUser(): Users
    {
        return $this->registeredUser;
    }
}
