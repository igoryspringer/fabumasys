<?php

namespace App\Service;

use App\Repository\CategoryRepository;

class CategoriesTree
{
    public function getCategories(CategoryRepository $categoryRepository)
    {
        $categories = $categoryRepository->findAll();

        $array_categories = [];
        foreach ($categories as $key => $category) {
            $array_categories[$category->getId()] = [
                'id' => $category->getId(),
                'name' => $category->getName(),
                'unit' => $category->getUnit(),
                'parentId' => $category->getParentId(),
            ];
        }

        return $array_categories;
    }

    public function getTree($data)
    {
        $tree = [];
        foreach ($data as $id => &$node) {
            if (!$node['parentId']) {
                $tree[$id] = &$node;
            } else {
                $data[$node['parentId']]['childs'][$id] = &$node;
            }
        }

        return $tree;
    }
}