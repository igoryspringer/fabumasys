<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Communal;
use App\Entity\Users;
use App\Repository\CategoryRepository;
use App\Repository\CommunalRepository;
use App\Service\CategoriesTree;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="first_page")
     */
    public function firstPageAction()
    {
        return $this->render('first_page/index.html.twig');
    }

    /**
     * @Route("/profile", name="main")
     *
     * @param CategoryRepository $categoryRepository
     * @param CommunalRepository $repository
     * @param CategoriesTree $categoriesTree
     * @return Response
     */
    public function index(CategoryRepository $categoryRepository, CommunalRepository $repository, CategoriesTree $categoriesTree)
    {
        $repoCommunal = $this->getDoctrine()->getRepository(Communal::class);
        $countInvoices = $repoCommunal->createQueryBuilder('a')
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $repoCategories = $this->getDoctrine()->getRepository(Category::class);
        $countCategories = $repoCategories->createQueryBuilder('a')
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $repoUsers = $this->getDoctrine()->getRepository(Users::class);
        $countUsers = $repoUsers->createQueryBuilder('a')
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $array_categories = $categoriesTree->getCategories($categoryRepository);
        $array_categories = $categoriesTree->getTree($array_categories);

        if (empty($categories)) {
            $namesCategories = null;
        }

        return $this->render('index.html.twig', [
            'countInvoices' => $countInvoices,
            'countCategories' => $countCategories,
            'countUsers' => $countUsers,
            'items' => $array_categories,
        ]);
    }
}
