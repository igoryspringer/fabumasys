<?php

namespace App\Controller;

use App\Entity\Communal;
use App\Form\CommunalType;
use App\Repository\CategoryRepository;
use App\Repository\CommunalRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/communal")
 */
class CommunalController extends AbstractController
{
    /**
     * @Route("/", name="communal_index", methods={"GET", "POST"})
     * @param CategoryRepository $categoryRepository
     * @param CommunalRepository $communalRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(CategoryRepository $categoryRepository, CommunalRepository $communalRepository, Request $request, PaginatorInterface $paginator): Response
    {
        if (!empty($request->request->getInt('val'))) {
            $lim = $request->request->getInt('val');
        } else {
            $lim = 10;
        }

        $communal = $communalRepository->findAll();

        $pagination = $paginator->paginate(
            $communal,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', $lim)
        );

        return $this->render('communal/index.html.twig', [
            'communals' => $pagination,
        ]);
    }

    /**
     * @Route("/new", name="communal_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $communal = new Communal();
        $form = $this->createForm(CommunalType::class, $communal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //dd($form->getData());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($communal);
            $entityManager->flush();

            return $this->redirectToRoute('communal_index');
        }

        return $this->render('communal/new.html.twig', [
            'communal' => $communal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="communal_show", methods={"GET"})
     * @param Communal $communal
     * @return Response
     */
    public function show(Communal $communal): Response
    {
        return $this->render('communal/show.html.twig', [
            'communal' => $communal,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="communal_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Communal $communal
     * @return Response
     */
    public function edit(Request $request, Communal $communal): Response
    {
        $form = $this->createForm(CommunalType::class, $communal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //dd($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('communal_index');
        }

        return $this->render('communal/edit.html.twig', [
            'communal' => $communal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="communal_delete", methods={"GET", "DELETE"})
     * @param Request $request
     * @param Communal $communal
     * @return Response
     */
    public function delete(Request $request, Communal $communal): Response
    {
        if ($this->isCsrfTokenValid('delete'.$communal->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($communal);
            $entityManager->flush();
        }

        return $this->redirectToRoute('communal_index');
    }

    /**
     * @Route("/ajax", name="ajax", methods={"POST"})
     * @param CommunalRepository $communalRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function ajax(CommunalRepository $communalRepository, Request $request, PaginatorInterface $paginator): Response
    {
        if (!empty($request->request->getInt('val'))) {
            $lim = $request->request->getInt('val');
        } else {
            $lim = 10;
        }

        $communal = $communalRepository->findAll();

        $pagination = $paginator->paginate(
            $communal,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', $lim)
        );

        return $this->render('communal/_table.html.twig', [
            'communals' => $pagination,
        ]);
    }

    /**
     * @Route("/search", name="search", methods={"POST"})
     * @param CommunalRepository $communalRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function search(CommunalRepository $communalRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $search = $request->request->get('search');

        if ('' == $search) {
            $communal = $communalRepository->findAll();
        } else {
            $communal = $communalRepository->findBySearchField($search);
        }

        if (!empty($request->request->getInt('val'))) {
            $lim = $request->request->getInt('val');
        } else {
            $lim = 10;
        }

        $pagination = $paginator->paginate(
            $communal,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', $lim)
        );

        return $this->render('communal/_table.html.twig', [
            'communals' => $pagination,
        ]);
    }
}
