<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use App\Repository\CommunalRepository;
use App\Service\CategoriesTree;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category")
 */
class CategoryController extends AbstractController
{
    protected $data;

    /**
     * @Route("/", name="category_index", methods={"GET"})
     * @param CategoryRepository $categoryRepository
     * @param CategoriesTree $categoriesTree
     * @return Response
     */
    public function index(CategoryRepository $categoryRepository, CategoriesTree $categoriesTree): Response
    {
        $array_categories = $categoriesTree->getCategories($categoryRepository);
        $tree_categories = $categoriesTree->getTree($array_categories);
        //echo "<pre>". print_r($tree_categories, true)."</pre>"; dd();
        return $this->render('category/index.html.twig', [
            'items' => $tree_categories,
        ]);
    }

    /**
     * @Route("/new", name="category_new", methods={"GET","POST"})
     * @param Request $request
     * @param CategoryRepository $categoryRepository
     * @param CategoriesTree $categoriesTree
     * @return Response
     */
    public function new(Request $request, CategoryRepository $categoryRepository, CategoriesTree $categoriesTree): Response
    {
        $array_categories = $categoriesTree->getCategories($categoryRepository);
        $tree_categories = $categoriesTree->getTree($array_categories);

        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('category_index');
        }

        return $this->render('category/new.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'items' => $tree_categories,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="category_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Category $category
     * @param CategoryRepository $categoryRepository
     * @param CategoriesTree $categoriesTree
     * @return Response
     */
    public function edit(Request $request, Category $category, CategoryRepository $categoryRepository, CategoriesTree $categoriesTree): Response
    {
        $array_categories = $categoriesTree->getCategories($categoryRepository);
        $tree_categories = $categoriesTree->getTree($array_categories);

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('category_index');
        }

        return $this->render('category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'items' => $tree_categories,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="category_delete", methods={"GET", "DELETE"})
     *
     * @param Request $request
     * @param Category $category
     * @param CategoryRepository $categoryRepository
     * @param CommunalRepository $communalRepository
     * @return Response
     * @throws Exception
     */
    public function delete(Request $request, Category $category, CategoryRepository $categoryRepository, CommunalRepository $communalRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $id = $category->getId();
            $allParentId = $categoryRepository->findByParentId($id);
            //$result = in_array($id, array_column($allParentId, 'parentId'));
            if ($allParentId) {
                throw new Exception('Cannot delete, categories have nested categories!');
            }
            $communal = $communalRepository->findByCategoryId($id);
            if ($communal) {
                throw new Exception('Cannot delete, categories have invoices!');
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirectToRoute('category_index');
    }

    /**
     * @Route("/{id}", name="category_show", methods={"GET"})
     * @param Category $category
     * @return Response
     */
    public function show(Category $category): Response
    {
        return $this->render('category/show.html.twig', [
            'category' => $category,
        ]);
    }
}
