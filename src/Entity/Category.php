<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="unit", type="string", length=255, nullable=false)
     */
    private $unit;
    /**
     * @ORM\Column(name="parentId", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Communal", mappedBy="category")
     */
    private $communals;

    public function __construct()
    {
        $this->communals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setParentId(int $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return Collection|Communal[]
     */
    public function getCommunals(): Collection
    {
        return $this->communals;
    }

    public function addCommunal(Communal $communal): self
    {
        if (!$this->communals->contains($communal)) {
            $this->communals[] = $communal;
            $communal->setCategory($this);
        }

        return $this;
    }

    public function removeCommunal(Communal $communal): self
    {
        if ($this->communals->contains($communal)) {
            $this->communals->removeElement($communal);
            // set the owning side to null (unless already changed)
            if ($communal->getCategory() === $this) {
                $communal->setCategory(null);
            }
        }

        return $this;
    }
}
