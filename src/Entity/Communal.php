<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="communal")
 *
 * @ORM\Entity(repositoryClass="App\Repository\CommunalRepository")
 *
 * @//Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false, hardDelete=true)
 */
class Communal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", nullable=false, type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="unit", nullable=true, type="string")
     */
    private $unit;

    /**
     * @ORM\Column(name="price", type="float", nullable=false)
     *
     * @Assert\Regex(
     *     pattern="/^[0-9]+(\.[0-9]{1,2})?$/u",
     *     match=true,
     *     message="Imput float: 1234.56")
     */
    private $price;

    /**
     * @ORM\Column(name="counter", nullable=true, type="float")
     */
    private $counter;

    /**
     * @ORM\Column(name="quantity", nullable=true, type="float")
     */
    private $quantity;

    /**
     * @ORM\Column(name="period", nullable=true, type="string")
     */
    private $period;

    /**
     * @ORM\Column(name="total", nullable=true, type="float")
     */
    private $total;

    /**
     * @ORM\Column(name="status", nullable=true, type="integer")
     */
    private $status = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @ORM\Column(name="iddocument", nullable=true, type="string", length=255)
     */
    private $iddocument;

    /**
     * @ORM\Column(name="datedocument", nullable=true, type="datetime")
     */
    private $datedocument;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="communals")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $category;

    public function __construct()
    {
        $this->created_at = new \DateTime('now');

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUnit(): ?float
    {
        return $this->unit;
    }

    public function setUnit(float $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCounter(): ?float
    {
        return $this->counter;
    }

    public function setCounter(float $counter): self
    {
        $this->counter = $counter;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(float $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getIddocument(): ?string
    {
        return $this->iddocument;
    }

    public function setIddocument(string $iddocument): self
    {
        $this->iddocument = $iddocument;

        return $this;
    }

    public function getDatedocument(): ?\DateTimeInterface
    {
        return $this->datedocument;
    }

    public function setDatedocument(?\DateTimeInterface $datedocument): self
    {
        $this->datedocument = $datedocument;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
