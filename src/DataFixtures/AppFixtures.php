<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Communal;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var EntityManagerInterface
     * @var UserPasswordEncoderInterface
     */
    private $em;
    private $encoder;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $entityManager;
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new Users();
        $user->setEmail('admin@admin.ua');
        $password = $this->encoder->encodePassword($user, 'admin');
        $user->setPassword($password);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEnable(1);
        $manager->persist($user);
        $user = new Users();
        $user->setEmail('user@user.ua');
        $password = $this->encoder->encodePassword($user, 'user');
        $user->setPassword($password);
        $user->setRoles(['ROLE_USER']);
        $user->setEnable(1);
        $manager->persist($user);

        $category = new Category();
        $category->setName('communal');
        $category->setUnit('none');
        $category->setParentId(0);
        $manager->persist($category);
        $category = new Category();
        $category->setName('communication');
        $category->setUnit('none');
        $category->setParentId(0);
        $manager->persist($category);
        $category = new Category();
        $category->setName('entertainment');
        $category->setUnit('none');
        $category->setParentId(0);
        $manager->persist($category);
        $manager->flush();

        $categoryId = $this->em->getRepository(Category::class)->findOneByName('communal')->getId();
        $category = new Category();
        $category->setName('electric');
        $category->setUnit('kW');
        $category->setParentId($categoryId);
        $manager->persist($category);
        $category = new Category();
        $category->setName('water');
        $category->setUnit('m3');
        $category->setParentId($categoryId);
        $manager->persist($category);
        $category = new Category();
        $category->setName('flat');
        $category->setUnit('m2');
        $category->setParentId($categoryId);
        $manager->persist($category);
        $categoryId = $this->em->getRepository(Category::class)->findOneByName('communication')->getId();
        $category = new Category();
        $category->setName('mobile');
        $category->setUnit('tariff');
        $category->setParentId($categoryId);
        $manager->persist($category);
        $category = new Category();
        $category->setName('internet');
        $category->setUnit('tariff');
        $category->setParentId($categoryId);
        $manager->persist($category);
        $categoryId = $this->em->getRepository(Category::class)->findOneByName('entertainment')->getId();
        $category = new Category();
        $category->setName('cafe');
        $category->setUnit('sum');
        $category->setParentId($categoryId);
        $manager->persist($category);
        $category = new Category();
        $category->setName('cinema');
        $category->setUnit('ticket');
        $category->setParentId($categoryId);
        $manager->persist($category);
        $manager->flush();

        $array_categories = $this->em->getRepository(Category::class)->findAllId();
        for ($i = 0; $i < 50; ++$i) {
            $id_category = array_rand($array_categories, 1);
            $category = $array_categories[$id_category];
            $categoryId = $category['id'];
            $category = $this->em->getRepository(Category::class)->findOneBy([
                'id' => $categoryId,
            ]);
            $communal = new Communal();
            $communal->setName(substr(md5(time().$i), 1, 5));
            $communal->setUnit(mt_rand(1, 100));
            $communal->setPrice(mt_rand(1, 100));
            $communal->setCounter(mt_rand(1, 1000));
            $communal->setQuantity(mt_rand(1, 500));
            $communal->setPeriod(random_int(1, 12));
            $communal->setTotal(mt_rand(1, 10000));
            $communal->setIddocument(substr(md5(time().$i), 1, 3));
            $communal->setDatedocument(\DateTime::createFromFormat('Y-m-d', '2002-02-02'));
            $communal->setCategory($category);
            $manager->persist($communal);
        }
        $manager->flush();
    }
}
